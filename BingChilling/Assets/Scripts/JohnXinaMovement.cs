using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;

public class JohnXinaMovement : MonoBehaviour
{
    bool justRotated = false;
    bool changeDirection = false;
    float characterDetectionRangeFront = 20f;
    float characterDetectionRangeSide = 10f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody>().velocity = transform.right * 10;
        float maxDistance = 5f;
        RaycastHit hitFront;
        RaycastHit hitRight;
        RaycastHit hitLeft;
        bool isHit = Physics.BoxCast(transform.position, transform.localScale/ 2, transform.right, out hitFront, transform.rotation, maxDistance);
        bool collisionLeft = Physics.BoxCast(transform.position, transform.localScale / 2, transform.forward, out hitRight, new Quaternion(0, 90, 0, 0), maxDistance * 2.5f);
        bool collisionRight = Physics.BoxCast(transform.position, transform.localScale / 2, -transform.forward, out hitLeft, new Quaternion(0, 90, 0, 0), maxDistance * 2.5f);
        if (isHit)
        {
            print("giro");
            if (!justRotated && hitFront.transform.tag != "Player")
            {
                
                if (!collisionLeft && !collisionRight)
                {
                    int r = Random.Range(0,2);
                    switch (r)
                    {
                        case 0:
                            transform.Rotate(new Vector3(0, -90, 0));
                            break;
                        case 1:
                            transform.Rotate(new Vector3(0, 90, 0));
                            break;
                        default:
                            break;
                    }
                }else if(!collisionLeft)
                {
                    transform.Rotate(new Vector3(0, -90, 0));
                }else if(!collisionRight)
                {
                    transform.Rotate(new Vector3(0, 90, 0));
                }else
                {
                    transform.Rotate(new Vector3(0, 180, 0));
                }
                StartCoroutine(rotateCD());
            }
        }else if (!collisionLeft)
        {
            if(!changeDirection)
            {
                int r = Random.Range(0, 2);
                if(r == 0)
                {
                    transform.Rotate(new Vector3(0, -90, 0));
                }else
                {
                    StartCoroutine(changeDirectionCD());
                }
            }
        }else if (!collisionRight)
        {
            if(!changeDirection)
            {
                int r = Random.Range(0, 2);
                if(r == 0)
                {
                    transform.Rotate(new Vector3(0, 90, 0));
                }else
                {
                    StartCoroutine(changeDirectionCD());
                }
            }
        }
        RaycastHit hitFront2;
        RaycastHit hitRight2;
        RaycastHit hitLeft2;
        bool characterHitFront = Physics.BoxCast(transform.position, transform.localScale / 2, transform.right, out hitFront2, transform.rotation, characterDetectionRangeFront);
        bool characterHitLeft = Physics.BoxCast(transform.position, transform.localScale / 2, transform.forward, out hitRight2, new Quaternion(0, 90, 0, 0), characterDetectionRangeSide);
        bool characterHitRight = Physics.BoxCast(transform.position, transform.localScale / 2, -transform.forward, out hitLeft2, new Quaternion(0, 90, 0, 0), characterDetectionRangeSide);
        if(hitFront2.transform.tag == "Player")
        {
            this.GetComponent<Rigidbody>().velocity = transform.right * 15;
        }else if (hitLeft2.transform.tag == "Player")
        {
            this.GetComponent<Rigidbody>().velocity = transform.right * 15;
            transform.Rotate(new Vector3(0, -90, 0));
        }else if (hitRight2.transform.tag == "Player")
        {
            this.GetComponent<Rigidbody>().velocity = transform.right * 15;
            transform.Rotate(new Vector3(0, 90, 0));
        }

    }
    IEnumerator changeDirectionCD()
    {
        changeDirection = true;
        yield return new WaitForSeconds(5f);
        changeDirection = false;
    }
    IEnumerator rotateCD()
    {
        justRotated=true;
        yield return new WaitForSeconds(0.1f);
        justRotated=false;
    }
    private void OnDrawGizmos()
    {
        float maxDistance = 10f;
        RaycastHit hit;
        
        bool isHit = Physics.BoxCast(transform.position, transform.localScale / 2, transform.right, out hit, transform.rotation, maxDistance);
        if (!isHit)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, transform.right*maxDistance);
            Gizmos.DrawWireCube(transform.position + transform.right * maxDistance, new Vector3(7, 7, 5));
            Gizmos.DrawWireCube(transform.position + transform.forward * maxDistance, new Vector3(5, 7, 7));
            Gizmos.DrawWireCube(transform.position + -transform.forward * maxDistance, new Vector3(5, 7, 7));
        }else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(transform.position + transform.right * hit.distance, new Vector3(7, 7, 5));
            Gizmos.DrawWireCube(transform.position + transform.forward * maxDistance, new Vector3(7, 7, 5));
            Gizmos.DrawWireCube(transform.position + -transform.forward * maxDistance, new Vector3(7, 7, 5));
            Gizmos.DrawRay(transform.position, transform.right * hit.distance);

        }
    }
}
