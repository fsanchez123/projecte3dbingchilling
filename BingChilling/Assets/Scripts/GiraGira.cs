using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiraGira : MonoBehaviour
{
    bool dir;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SubeBaja());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 0.1f, 0));
        if (dir) { 
            transform.Translate(new Vector3(0, 0.001f, 0));
        }else
        {
            transform.Translate(new Vector3(0, -0.001f, 0));
        }
    }

    IEnumerator SubeBaja()
    {
        while (true)
        {
            dir = false;
            yield return new WaitForSeconds(1);
            dir = true;
            yield return new WaitForSeconds(1);
        }
    }
}
