using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerScript : MonoBehaviour
{

    Rigidbody rb;
    [SerializeField]
    int spd;
    public int rotationSpd;
    public GameObject dir;
    int SocialCreditsCollected = 0;
    bool grounded = false;
    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
        {
            direction = dir.transform.forward * spd;
        }
        if (Input.GetKey(KeyCode.S))
        {
            direction = -dir.transform.forward * spd;
        }
        if (Input.GetKey(KeyCode.A))
        {
            direction = -dir.transform.right * spd;
        }
        if (Input.GetKey(KeyCode.D))
        {
            direction = dir.transform.right * spd;
        }
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            grounded = false;
            rb.AddForce(new Vector3(0,400,0));
        }
        rb.velocity = new Vector3(direction.x, rb.velocity.y, direction.z);

        direction = new Vector3(rb.velocity.x,0,rb.velocity.z).normalized;
        if (direction != Vector3.zero)
        {
            Quaternion rotateTo = Quaternion.LookRotation(direction, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotateTo, rotationSpd * Time.deltaTime);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "JohnXina")
        {
            SceneManager.LoadScene("GameOverLost");
        }else if(collision.transform.tag == "SocialCredit")
        {
            SocialCreditsCollected++;
            Destroy(collision.gameObject, 0.1f);
            if(SocialCreditsCollected == 10)
            {
                SceneManager.LoadScene("GameOvertWin");
            }
        }
        else if (collision.transform.tag == "Suelo")
        {
            grounded = true;
        }else if (collision.transform.tag == "Lava")
        {
            SceneManager.LoadScene("GameOverLost");
        }
    }
}
