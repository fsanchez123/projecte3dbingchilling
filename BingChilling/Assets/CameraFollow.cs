using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed;
    public Vector3 offset;
    float rotationY = 0f;

    float sens = 2;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        Vector3 position = target.position + offset;
        transform.position = position;
        rotationY += Input.GetAxis("Mouse X") * sens;
        transform.localEulerAngles = new Vector3(0,rotationY,0);
    }
}
